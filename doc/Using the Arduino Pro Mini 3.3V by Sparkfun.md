# *Using the Arduino Pro Mini 3.3V*

[Using the Arduino Pro Mini 3.3V](https://learn.sparkfun.com/tutorials/using-the-arduino-pro-mini-33v)

Do not forget to set the FTDI breakout jumpers to the correct voltage of the Arduino used.
