# Configure includePath for better IntelliSense results

[Configuring includePath for better IntelliSense results](https://github.com/Microsoft/vscode-cpptools/blob/master/Documentation/Getting%20started%20with%20IntelliSense%20configuration.md)

This is for when Ctrl+clicking through definitions of Arduino builtin functions.
