# Arduino

## Installing

[Download](https://www.arduino.cc/en/Main/Software) the desktop application, not the web application agent.
The desktop application is a prerequisite for the Visual Studio Code Arduino extension and operates on files so Git can be used.
The web application relies on installed agent which still invokes the tools locally, but the files are in the cloud, so Got cannot be used.

In the Arduino IDE, go to File>Preferences>Use different IDE to make Arduino IDE readonly and indicate another tool is used.

[Install](https://github.com/Microsoft/vscode-arduino#installation) the Visual Studio Code Arduino extension.

## Configuring

[Command reference](https://github.com/Microsoft/vscode-arduino#commands)

- Check *Arduino: Board Manager* to see if boards are installed and up-to-date
- Use *Arduino: Library Manager* to install additional libraries (e.g.: `FastLED`)
- Use *Arduino: Change Board Type* to select the board used (e.g.: *Arduino Pro or Pro Mini*)
- Use *Arduino: Select Serial Port* to select the correct serial port

## Uploading

- Use *Arduino: Upload*

## Contributing

My Arduino projects:

- [Carpenter Brut Tee](https://gitlab.com/TomasHubelbauer/carpenter-brut-tee)
